#!/bin/bash

cd /tmp/ || exit

# Basic Installs/Updates

microdnf update
microdnf install git jq unzip

microdnf module enable nodejs:18
microdnf install nodejs

# Install AWS CLI

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install

# Cleanup

microdnf clean all
rm -rf /tmp/aws/
rm -rf /var/cache/dnf
rm -rf /var/cache/yum